Requires Android Studio 3.3 beta or above.

# TODO:
* PlaybackService needs to implement MediaBrowserServiceCompat media browsing functionality; ExoPlayer should then play media by id and not by url.
* Tests.
* Display exception (a consumable event) & retry request logic.
* Logic should be moved from fragments to presenters.
