package com.yanchenko.mimimi.repo

import androidx.annotation.UiThread
import androidx.paging.PagedList
import com.yanchenko.mimimi.Artist
import com.yanchenko.mimimi.ServerTrack
import com.yanchenko.mimimi.Track
import com.yanchenko.mimimi.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.coroutines.CoroutineContext

typealias TrackCallback = (Pair<Artist?, Track>) -> Unit
typealias ExceptionCallback = (Throwable) -> Unit

class BoundaryCallback(
	override val coroutineContext: CoroutineContext,
	private val nextPage: () -> Int,
	private val loadTracks: (pasge: Int) -> Deferred<List<ServerTrack>>,
	private val loadWaveForm: (ServerTrack) -> Deferred<ByteArray>,
	private val loadArtist: (User) -> Deferred<Artist>?,
	private val trackLoadedCallback: TrackCallback,
	private val exceptionCallback: ExceptionCallback
) : PagedList.BoundaryCallback<Artist>(), CoroutineScope {

	private val initialLoading = AtomicBoolean(false)
	private val nextPageLoading = AtomicBoolean(false)

	@UiThread
	override fun onZeroItemsLoaded() {
		checkAndLoad(initialLoading) { 0 }
	}

	@UiThread
	override fun onItemAtEndLoaded(itemAtEnd: Artist) {
		checkAndLoad(nextPageLoading, nextPage)
	}

	private fun checkAndLoad(loading: AtomicBoolean, page: () -> Int) = launch {
		if (!loading.getAndSet(true))
			try {
				loadPage(page())
			} catch (t: Throwable) {
				exceptionCallback(t)
			}
		loading.set(false)
	}

	private suspend fun loadPage(page: Int) = coroutineScope {
		val serverTracks = loadTracks(page).await()
		serverTracks.forEach {
			//TODO waveformdata should be loaded & stored the first time the track is displayed
			val waveFormdata = loadWaveForm(it).await()
			val track = Track.convert(it, waveFormdata)
			val artist = loadArtist(it.user)?.await()
			trackLoadedCallback(artist to track)
		}
	}
}
