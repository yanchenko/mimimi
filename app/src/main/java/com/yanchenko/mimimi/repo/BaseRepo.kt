package com.yanchenko.mimimi.repo

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

abstract class BaseRepo : CoroutineScope {

	private val job = Job()

	fun cancel() = job.cancel()

	override val coroutineContext = Dispatchers.IO + job

}
