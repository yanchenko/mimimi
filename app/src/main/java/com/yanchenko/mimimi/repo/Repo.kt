package com.yanchenko.mimimi.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.yanchenko.mimimi.*
import com.yanchenko.mimimi.api.HearthisApi
import com.yanchenko.mimimi.api.Type
import com.yanchenko.mimimi.db.Db
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicInteger

class Repo(
	private val api: HearthisApi,
	private val db: Db
) : BaseRepo() {

	companion object {
		const val PAGE_SIZE = 20
	}

	private val page = AtomicInteger()
	private val exception = MutableLiveData<Throwable>()

	fun artists(): RepoData<Artist> {
		val boundaryCallback = BoundaryCallback(
			coroutineContext = coroutineContext,
			nextPage = page::getAndIncrement,
			loadTracks = ::loadTracks,
			loadWaveForm = ::loadWaveForm,
			loadArtist = ::loadArtistIfNeeded,
			trackLoadedCallback = ::persistArtistAndTrack,
			exceptionCallback = ::exceptionCallback
		)
		return RepoData(
			resltList = db.artists().all().toLiveData(pageSize = PAGE_SIZE, boundaryCallback = boundaryCallback),
			loadException = exception
		)
	}

	fun removeAll() = launch {
		page.set(0)
		db.runInTransaction {
			// tracks will be deleted b/c of foreign key relation
//			db.tracks().deleteAll()
			db.artists().deleteAll()
		}
	}

	fun tracksForArtist(id: String) =
		db.tracks().forArtistWithId(id)

	private fun loadTracks(page: Int) = api.tracks(Type.popular, PAGE_SIZE, page)
	private fun loadWaveForm(track: ServerTrack) = api.waveFormData(track.waveformData)

	private fun loadArtistIfNeeded(user: User): Deferred<Artist>? {
		return if (db.artists().gotWithId(user.id)) {
			null
		} else {
			api.artist(user.permalink)
		}
	}

	private fun persistArtistAndTrack(artistToTrack: Pair<Artist?, Track>) {
		val (artist, track) = artistToTrack
		artist?.run(db.artists()::insert)
		db.tracks().insert(track)
	}

	private fun exceptionCallback(t: Throwable) {
		exception.postValue(t)
	}

}

data class RepoData<T : WithId>(
	val resltList: LiveData<PagedList<T>>,
	val loadException: LiveData<Throwable>
)
