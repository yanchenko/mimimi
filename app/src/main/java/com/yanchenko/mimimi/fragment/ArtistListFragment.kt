package com.yanchenko.mimimi.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.fragment.app.transaction
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.squareup.picasso.Picasso
import com.yanchenko.mimimi.Artist
import com.yanchenko.mimimi.R
import com.yanchenko.mimimi.adapter.ArtistListAdapter
import com.yanchenko.mimimi.vm.ArtistListViewModel
import kotlinx.android.synthetic.main.fragment_artist_list.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ArtistListFragment : BaseFragment(R.layout.fragment_artist_list) {

	private val vm by viewModel<ArtistListViewModel>()
	private val imageLoader by inject<Picasso>()

	private val adapter by lazy { ArtistListAdapter(imageLoader, ::displayTracksForArtist) }

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		viewArtistList.apply {
			layoutManager = StaggeredGridLayoutManager(2, VERTICAL)
			adapter = this@ArtistListFragment.adapter
		}
		vm.artists.observe(this, Observer(adapter::submitList))
		vm.displayLoadingIndicator.observe(this, Observer(viewSwipeRefresh::setRefreshing))
		viewSwipeRefresh.setOnRefreshListener { vm.refresh() }
	}

	//

	private fun displayTracksForArtist(artist: Artist, transitionFromView: View?) {
		val viewFrom = transitionFromView!!
		val transitionName = ViewCompat.getTransitionName(viewFrom)!!
		val fragment = TrackListFragment.newInstance(artist, transitionName)
		supportFragmentManager.transaction(allowStateLoss = true) {
			//FIXME
			addSharedElement(viewFrom, transitionName)
			replace(R.id.fragmentContainer, fragment)
			addToBackStack(null)
		}
	}
}
