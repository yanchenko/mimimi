package com.yanchenko.mimimi.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class BaseFragment(@LayoutRes private val viewResId: Int) : Fragment() {

	final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
		layoutInflater.inflate(viewResId, container, false)

	protected val supportFragmentManager by lazy { activity!!.supportFragmentManager }

}