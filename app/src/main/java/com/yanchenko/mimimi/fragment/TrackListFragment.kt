package com.yanchenko.mimimi.fragment

import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import com.yanchenko.mimimi.Artist
import com.yanchenko.mimimi.R
import com.yanchenko.mimimi.adapter.TrackListAdapter
import com.yanchenko.mimimi.vm.TrackListViewModel
import kotlinx.android.synthetic.main.fragment_track_list.*
import kotlinx.android.synthetic.main.include_title_subtitle.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class TrackListFragment : BaseFragment(R.layout.fragment_track_list) {

	companion object {

		private val ARTIST = "artist"
		private val TRANSITION_NAME = "transition_name"

		fun newInstance(artist: Artist, transitionName: String) = TrackListFragment().apply {
			arguments = Bundle().apply {
				putSerializable(ARTIST, artist)
				putString(TRANSITION_NAME, transitionName)
			}
		}
	}

	private val vm by viewModel<TrackListViewModel>()

	private val adapter by lazy { TrackListAdapter(vm::playTrack) }
	private val imageLoader by inject<Picasso>()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		postponeEnterTransition()
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
		}
	}

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		val artist = arguments?.getSerializable(ARTIST) as Artist
		val transitionName = arguments?.getString(TRANSITION_NAME)
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			viewImgHeader.transitionName = transitionName
		}
		viewTitle.text = artist.username
		viewSubtitle.text = artist.description
		imageLoader.load(artist.avatarUrl).into(viewImgHeader)
		viewTrackList.apply {
			layoutManager = LinearLayoutManager(context)
			addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
			adapter = this@TrackListFragment.adapter
		}
		vm.tracksForArtist(artist.id).observe(this, Observer(adapter::submitList))
	}

}