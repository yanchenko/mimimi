package com.yanchenko.mimimi.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.yanchenko.mimimi.Track

@Dao
interface TrackDao {

	@Insert(onConflict = REPLACE)
	fun insert(item: Track)

	@Query("SELECT * FROM tracks WHERE artistId = :id")
	fun forArtistWithId(id: String): LiveData<List<Track>>

	@Query("DELETE FROM tracks")
	fun deleteAll()
}