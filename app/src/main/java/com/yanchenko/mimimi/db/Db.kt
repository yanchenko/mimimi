package com.yanchenko.mimimi.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.yanchenko.mimimi.Artist
import com.yanchenko.mimimi.Track

@Database(
	entities = [Track::class, Artist::class],
	version = 1,
	exportSchema = false
)
abstract class Db : RoomDatabase() {

	abstract fun artists(): ArtistDao

	abstract fun tracks(): TrackDao

	companion object {

		fun create(ctx: Context, inMemory: Boolean = true) =
			if (inMemory) {
				Room.inMemoryDatabaseBuilder(ctx, Db::class.java)
			} else {
				Room.databaseBuilder(ctx, Db::class.java, "db.sqlite")
			}.fallbackToDestructiveMigration().build()

	}
}
