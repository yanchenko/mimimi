package com.yanchenko.mimimi.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.yanchenko.mimimi.Artist

@Dao
interface ArtistDao {

	@Insert(onConflict = REPLACE)
	fun insert(item: Artist)

	@Query("SELECT COUNT(*) FROM artists WHERE id = :id")
	fun gotWithId(id: String): Boolean

	@Query("SELECT * FROM artists WHERE id = :id")
	fun forId(id: String): Artist?

	@Query("SELECT * FROM artists")
	fun all(): DataSource.Factory<Int, Artist>

	@Query("DELETE FROM artists")
	fun deleteAll()
}