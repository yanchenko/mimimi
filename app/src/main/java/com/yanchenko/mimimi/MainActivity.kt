package com.yanchenko.mimimi

import android.media.AudioManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.transaction
import com.google.android.material.snackbar.Snackbar
import com.yanchenko.mimimi.fragment.ArtistListFragment
import com.yanchenko.mimimi.playback.PlaybackHandler
import com.yanchenko.mimimi.vm.ArtistListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

	private val vm by viewModel<ArtistListViewModel>()

	private val playbackHandler by inject<PlaybackHandler>()

	//

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		volumeControlStream = AudioManager.STREAM_MUSIC
		if (savedInstanceState == null) {
			supportFragmentManager.transaction {
				add(R.id.fragmentContainer, ArtistListFragment())
			}
		}
		lifecycle.addObserver(playbackHandler)
//		vm.loadError.observe(this, Observer { it?.run(::displayError) })
	}


	private fun displayError(txt: String) =
		Snackbar.make(coordinatorLayout, txt, Snackbar.LENGTH_LONG).show()

}
