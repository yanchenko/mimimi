package com.yanchenko.mimimi.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.yanchenko.mimimi.Artist
import com.yanchenko.mimimi.ServerTrack
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

interface HearthisApi {

	@GET("/feed")
	fun tracks(@Query("type") type: Type, @Query("page") page: Int, @Query("count") count: Int): Deferred<List<ServerTrack>>

	@GET("/{permalink}")
	fun artist(@Path("permalink") permalink: String): Deferred<Artist>

	@GET
	fun waveFormData(@Url url: String): Deferred<ByteArray>

	companion object {

		private const val BASE_URL = "https://api-v2.hearthis.at"

		fun create(baseUrl: String = BASE_URL, log: Boolean = true): HearthisApi {

			val client = OkHttpClient.Builder()
				.apply {
					if (log)
						HttpLoggingInterceptor()
							.apply { level = HttpLoggingInterceptor.Level.BODY }
							.run(::addInterceptor)
				}
				.build()

			return Retrofit.Builder().baseUrl(baseUrl)
				.client(client)
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(CoroutineCallAdapterFactory())
				.build()
				.create(HearthisApi::class.java)
		}
	}

}

enum class Type {
	popular
}