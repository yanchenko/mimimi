package com.yanchenko.mimimi

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

interface WithId : Serializable {
	fun itemId(): String
}

data class ServerTrack(
	val id: String,
	val title: String,
	@SerializedName("stream_url")
	val streamUrl: String,
	@SerializedName("waveform_data")
	val waveformData: String,
	val user: User
)

data class User(
	val id: String,
	val username: String,
	val permalink: String
)

@Entity(
	tableName = "tracks",
	foreignKeys = [ForeignKey(
		entity = Artist::class,
		parentColumns = ["id"],
		childColumns = ["artistId"],
		onDelete = CASCADE
	)]
)
data class Track(
	@PrimaryKey
	val id: String,
	val title: String,
	val streamUrl: String,
	val artistId: String,
	val waveformData: ByteArray
) : WithId {
	override fun itemId() = id

	companion object {
		fun convert(from: ServerTrack, waveformData: ByteArray) =
			Track(from.id, from.title, from.streamUrl, from.user.id, waveformData)
	}
}

@Entity(
	tableName = "artists",
	indices = [Index("permalink", unique = true)]
)
data class Artist(
	@PrimaryKey
	val id: String,
	val permalink: String,
	val username: String,
	val description: String,
	@SerializedName("avatar_url")
	val avatarUrl: String,
	@SerializedName("background_url")
	val backgroundUrl: String,
	@SerializedName("track_count")
	val trackCount: Int
) : WithId {
	override fun itemId() = id
}