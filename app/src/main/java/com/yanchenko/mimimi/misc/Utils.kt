package com.yanchenko.mimimi.misc

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

fun <T> load(task: Deferred<T>): LiveData<Result<T>> =
	MutableLiveData<Result<T>>().also { liveData ->
		GlobalScope.launch {
			try {
				val result = task.await()
				Result.success(result)
			} catch (e: Throwable) {
				Result.failure<T>(e)
			}.run { liveData.postValue(this) }
		}
	}

fun <T> flatMapIterable(src: LiveData<out Iterable<T>>) =
	MediatorLiveData<T>().apply { addSource(src) { it.forEach(::setValue) } }
