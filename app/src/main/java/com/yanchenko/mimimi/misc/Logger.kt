package com.yanchenko.mimimi.misc

import android.util.Log

/**
 * Created by Alex Yanchenko <alex@yanchenko.com> on 12/30/17.
 */

//inline fun <reified T : Any> Logger() = Logger(T::class.java)

class Logger(private val tag: String) {

	constructor(cls: Class<*>) : this("${cls.simpleName}::class")

	fun v(msg: Any?, tr: Throwable? = null) =
		log(Log.VERBOSE, msg, tr)

	fun d(msg: Any?, tr: Throwable? = null) =
		log(Log.DEBUG, msg, tr)

	fun i(msg: Any?, tr: Throwable? = null) =
		log(Log.INFO, msg, tr)

	fun w(msg: Any?, tr: Throwable? = null) =
		log(Log.WARN, msg, tr)

	fun e(msg: Any?, tr: Throwable? = null) =
		log(Log.ERROR, msg, tr)

	fun wtf(msg: Any?, tr: Throwable? = null) =
		log(Log.ASSERT, msg, tr)

	private fun log(priority: Int, msg: Any?, tr: Throwable?): LogMsg {
		val message = StringBuilder().apply {
			append(msg)
			tr?.run {
				append('\n').append(Log.getStackTraceString(this))
			}

		}.toString()
		Log.println(priority, tag, message)
		return LogMsg(priority, tag, message)
	}

}

data class LogMsg(val priority: Int, val tag: String, val message: String)

fun wtf(obj: Any?) =
	Logger("WTF").wtf(obj, obj as? Throwable)
