package com.yanchenko.mimimi

import android.app.Application
import com.squareup.picasso.Picasso
import com.yanchenko.mimimi.api.HearthisApi
import com.yanchenko.mimimi.db.Db
import com.yanchenko.mimimi.playback.PlaybackHandler
import com.yanchenko.mimimi.repo.Repo
import com.yanchenko.mimimi.vm.ArtistListViewModel
import com.yanchenko.mimimi.vm.TrackListViewModel
import org.koin.android.ext.android.startKoin
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

class App : Application() {
	override fun onCreate() {
		super.onCreate()
		startKoin(this, listOf(appModule))
	}
}

val appModule = module {
	single { HearthisApi.create(log = true) }
	single { Db.create(get()) }
	single { Picasso.Builder(get()).build() }
	single { Repo(get(), get()) }
	single { PlaybackHandler(get()) }
	viewModel { ArtistListViewModel(get()) }
	viewModel { TrackListViewModel(get(), get()) }
}