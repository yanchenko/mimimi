package com.yanchenko.mimimi.vm

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.yanchenko.mimimi.Track
import com.yanchenko.mimimi.playback.PlaybackHandler
import com.yanchenko.mimimi.playback.PlaybackService
import com.yanchenko.mimimi.repo.Repo

class TrackListViewModel(private val repo: Repo, private val playbackHandler: PlaybackHandler) : BaseViewModel() {

	fun tracksForArtist(artistId: String) =
		repo.tracksForArtist(artistId)

	fun playTrack(track: Track, transitionFromView: View?) {
		playbackHandler.transportControls()?.run {
			val uri = Uri.parse(track.streamUrl)
			val extras = Bundle()
				.apply { putSerializable(PlaybackService.EXTRA_TRACK, track) }
			prepareFromUri(uri, extras)
		}
	}

}
