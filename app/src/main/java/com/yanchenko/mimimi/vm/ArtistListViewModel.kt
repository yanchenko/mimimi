package com.yanchenko.mimimi.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.yanchenko.mimimi.repo.Repo

class ArtistListViewModel(private val repo: Repo) : BaseViewModel() {

	val artists = repo.artists().resltList
	//TODO use this to display loading error
	val exception = repo.artists().loadException

	val displayLoadingIndicator: LiveData<Boolean> = MediatorLiveData<Boolean>().apply {
		addSource(artists) { value = it.isEmpty() }
	}

	fun refresh() {
		(displayLoadingIndicator as MutableLiveData).value = true
		repo.removeAll()
	}

}
