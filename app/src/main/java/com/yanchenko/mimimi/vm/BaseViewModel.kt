package com.yanchenko.mimimi.vm

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

abstract class BaseViewModel : ViewModel(), CoroutineScope {

	private val job = Job()

	override fun onCleared() = job.cancel()

	override val coroutineContext = Dispatchers.Main + job

}
