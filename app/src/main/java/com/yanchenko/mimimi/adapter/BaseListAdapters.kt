package com.yanchenko.mimimi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yanchenko.mimimi.WithId
import kotlinx.android.extensions.LayoutContainer

abstract class SimpleListAdapter<T : WithId, VH : SimpleViewHolder<T>> :
	ListAdapter<T, VH>(diffItemCallback()), ListAdapterBase<T, VH> {

	final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
		createViewHolderDelegate(this, onItemClick, parent)

	final override fun onBindViewHolder(viewHolder: VH, position: Int) =
		bindViewHolderDelegate(::getItem, viewHolder, position)
}

abstract class SimplePagedListAdapter<T : WithId, VH : SimpleViewHolder<T>> :
	PagedListAdapter<T, VH>(diffItemCallback()), ListAdapterBase<T, VH> {

	final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
		createViewHolderDelegate(this, onItemClick, parent)

	final override fun onBindViewHolder(viewHolder: VH, position: Int) =
		bindViewHolderDelegate(::getItem, viewHolder, position)
}

abstract class SimpleViewHolder<T>(ctx: Context, @LayoutRes itemLayoutResId: Int) :
	RecyclerView.ViewHolder(inflateView(ctx, itemLayoutResId)), LayoutContainer {

	override val containerView = itemView
	var item: T? = null
	abstract val transitionFromView: View?
	abstract fun bindToItem(item: T?)
}

private interface ListAdapterBase<T : WithId, VH : SimpleViewHolder<T>> {
	fun createViewHolder(ctx: Context): VH
	val onItemClick: (T, View?) -> Unit
}

private fun <T : WithId> diffItemCallback() =
	object : DiffUtil.ItemCallback<T>() {
		override fun areItemsTheSame(oldItem: T, newItem: T) =
			oldItem.itemId() == newItem.itemId()

		override fun areContentsTheSame(oldItem: T, newItem: T) =
			oldItem == newItem
	}

private fun <T : WithId, VH : SimpleViewHolder<T>> createViewHolderDelegate(
	adapter: ListAdapterBase<T, VH>,
	onItemClick: (T, View?) -> Unit,
	parent: ViewGroup
) = adapter.createViewHolder(parent.context)
	.also { vh -> vh.itemView.setOnClickListener { vh.item?.run { onItemClick(this, vh.transitionFromView) } } }

private fun <T : WithId, VH : SimpleViewHolder<T>> bindViewHolderDelegate(
	getItem: (Int) -> T?,
	viewHolder: VH,
	position: Int
) {
	viewHolder.item = getItem(position)
	viewHolder.bindToItem(viewHolder.item)
}

private fun inflateView(ctx: Context, @LayoutRes itemLayoutResId: Int) =
	LayoutInflater.from(ctx).inflate(itemLayoutResId, null, false)
