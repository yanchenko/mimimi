package com.yanchenko.mimimi.adapter

import android.content.Context
import android.view.View
import androidx.core.view.ViewCompat
import com.squareup.picasso.Picasso
import com.yanchenko.mimimi.Artist
import com.yanchenko.mimimi.R
import kotlinx.android.synthetic.main.include_title_subtitle.*
import kotlinx.android.synthetic.main.row_artist.*

class ArtistListAdapter(private val imageLoader: Picasso, override val onItemClick: (Artist, View?) -> Unit) :
	SimplePagedListAdapter<Artist, ArtistViewHolder>() {

	override fun createViewHolder(ctx: Context) =
		ArtistViewHolder(ctx, imageLoader)

}

class ArtistViewHolder(private val ctx: Context, private val imageLoader: Picasso) :
	SimpleViewHolder<Artist>(ctx, R.layout.row_artist) {
	override val transitionFromView = viewImg

	override fun bindToItem(item: Artist?) {
		if (item != null) {
			viewTitle.text = item.username
			viewSubtitle.text = ctx.getString(R.string.tracks_format, item.trackCount)
			imageLoader.load(item.avatarUrl).into(viewImg)
			ViewCompat.setTransitionName(viewImg, item.username)
		} else {
			//TODO pretty loading display
			viewTitle.text = "Loading..."
			viewImg.setImageDrawable(null)
		}
	}
}
