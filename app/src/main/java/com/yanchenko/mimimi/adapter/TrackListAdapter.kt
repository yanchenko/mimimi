package com.yanchenko.mimimi.adapter

import android.content.Context
import android.view.View
import com.yanchenko.mimimi.R
import com.yanchenko.mimimi.Track
import kotlinx.android.synthetic.main.row_track.*

class TrackListAdapter(override val onItemClick: (Track, View?) -> Unit) :
	SimpleListAdapter<Track, TrackViewHolder>() {

	override fun createViewHolder(ctx: Context) =
		TrackViewHolder(ctx)

}

class TrackViewHolder(ctx: Context) :
	SimpleViewHolder<Track>(ctx, R.layout.row_track) {
	override val transitionFromView = null
	override fun bindToItem(item: Track?) {
		text1.text = item?.title ?: ""
		viewWave.updateVisualizer(item?.waveformData)
	}
}
