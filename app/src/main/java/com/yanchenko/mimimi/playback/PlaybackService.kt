package com.yanchenko.mimimi.playback

import android.app.Notification
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.ResultReceiver
import android.support.v4.media.MediaBrowserCompat.MediaItem
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import androidx.media.MediaBrowserServiceCompat
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.squareup.picasso.Picasso
import com.yanchenko.mimimi.Artist
import com.yanchenko.mimimi.MainActivity
import com.yanchenko.mimimi.R
import com.yanchenko.mimimi.Track
import com.yanchenko.mimimi.db.Db
import com.yanchenko.mimimi.misc.Logger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class PlaybackService : MediaBrowserServiceCompat(), CoroutineScope {
	override val coroutineContext = Dispatchers.IO

	companion object {
		val EXTRA_TRACK = "track"
	}

	private val log = Logger(javaClass)

	private val db by inject<Db>()
	private val imageLoader by inject<Picasso>()

	private val mediaSession by lazy {
		MediaSessionCompat(this, packageName)
	}

	private val player by lazy {
		ExoPlayerFactory.newSimpleInstance(this)
	}

	private val playerNotificationManager by lazy {
		PlayerNotificationManager.createWithNotificationChannel(
			this,
			"player_notification",
			R.string.app_name,
			1,
			descriptionAdapter
		).apply { setNotificationListener(notificationListener) }
	}

	private val dataSourceFactory by lazy {
		DefaultDataSourceFactory(this, Util.getUserAgent(this, getString(R.string.app_name)))
	}

	override fun onCreate() {
		super.onCreate()
		MediaSessionConnector(mediaSession)
			.setPlayer(player, playbackPreparer)
		playerNotificationManager.setPlayer(player)
		playerNotificationManager.setMediaSessionToken(mediaSession.sessionToken)
		sessionToken = mediaSession.sessionToken
		log.d("Media Session with token $sessionToken created.")
	}

	override fun onDestroy() {
		super.onDestroy()
		playerNotificationManager.setPlayer(null)
		player.release()
	}

	private var track: Track? = null
	private var artist: Artist? = null

	private val playbackPreparer = object : MediaSessionConnector.PlaybackPreparer {

		override fun getSupportedPrepareActions() =
			PlaybackStateCompat.ACTION_PREPARE_FROM_URI

		override fun getCommands() = null

		override fun onPrepareFromUri(uri: Uri, extras: Bundle) {
			track = extras.getSerializable(EXTRA_TRACK) as? Track
			log.i("Prepare $track")
			launch {
				artist = track?.artistId?.let(db.artists()::forId)
			}
			ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
				.run(player::prepare)
			//FIXME should handle playfromuri instead
			player.playWhenReady = true
		}

		override fun onPrepare() {
		}

		override fun onPrepareFromSearch(query: String, extras: Bundle) {
		}

		override fun onPrepareFromMediaId(mediaId: String, extras: Bundle) {
		}

		override fun onCommand(player: Player, command: String, extras: Bundle, cb: ResultReceiver) {
		}
	}

	private val descriptionAdapter = object : PlayerNotificationManager.MediaDescriptionAdapter {
		override fun createCurrentContentIntent(player: Player) =
			Intent(applicationContext, MainActivity::class.java).let {
				PendingIntent.getActivity(
					applicationContext,
					0, it, PendingIntent.FLAG_UPDATE_CURRENT
				)
			}

		override fun getCurrentContentText(player: Player) = artist?.username

		override fun getCurrentContentTitle(player: Player) = track?.title

		override fun getCurrentLargeIcon(
			player: Player,
			callback: PlayerNotificationManager.BitmapCallback
		): Bitmap? {
			launch {
				artist?.run {
					val bm = imageLoader.load(avatarUrl).get()
					callback.onBitmap(bm)
				}
			}
			return null
		}
	}

	private val notificationListener = object : PlayerNotificationManager.NotificationListener {
		override fun onNotificationCancelled(notificationId: Int) = stopSelf()

		override fun onNotificationStarted(notificationId: Int, notification: Notification) =
			startForeground(notificationId, notification)
	}

	//

	override fun onGetRoot(clientPackageName: String, clientUid: Int, rootHints: Bundle?) =
		BrowserRoot("media_root_id", null)

	override fun onLoadChildren(parentId: String, result: Result<MutableList<MediaItem>>) {
		result.sendResult(mutableListOf())
	}
}