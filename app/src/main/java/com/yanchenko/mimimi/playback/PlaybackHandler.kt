package com.yanchenko.mimimi.playback

import android.content.ComponentName
import android.content.Context
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.session.MediaControllerCompat
import androidx.lifecycle.Lifecycle.Event
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

class PlaybackHandler(private val ctx: Context) : LifecycleObserver {

	private lateinit var mediaBrowser: MediaBrowserCompat

	private val mediaBrowserConnectionCallback = object : MediaBrowserCompat.ConnectionCallback() {
		override fun onConnected() {
			val sessionToken = mediaBrowser.sessionToken
			mediaController = MediaControllerCompat(ctx, sessionToken)
		}
	}

	private var mediaController: MediaControllerCompat? = null

	fun transportControls() = mediaController?.transportControls

	@OnLifecycleEvent(Event.ON_CREATE)
	private fun onCreate() {
		mediaBrowser = MediaBrowserCompat(
			ctx,
			ComponentName(ctx, PlaybackService::class.java),
			mediaBrowserConnectionCallback,
			null
		)
	}

	@OnLifecycleEvent(Event.ON_START)
	private fun onStart() {
		mediaBrowser.connect()
	}

}
